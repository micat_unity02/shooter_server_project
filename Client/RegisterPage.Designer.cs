﻿namespace Client
{
	partial class RegisterPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			TextBox_ID = new System.Windows.Forms.TextBox();
			TextBox_PW = new System.Windows.Forms.TextBox();
			label3 = new System.Windows.Forms.Label();
			TextBox_Nickname = new System.Windows.Forms.TextBox();
			label4 = new System.Windows.Forms.Label();
			Button_Register = new System.Windows.Forms.Button();
			SuspendLayout();
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Font = new System.Drawing.Font("맑은 고딕", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			label1.Location = new System.Drawing.Point(19, 9);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(160, 47);
			label1.TabIndex = 0;
			label1.Text = "회원가입";
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(12, 80);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(19, 15);
			label2.TabIndex = 1;
			label2.Text = "ID";
			// 
			// TextBox_ID
			// 
			TextBox_ID.Location = new System.Drawing.Point(79, 77);
			TextBox_ID.Name = "TextBox_ID";
			TextBox_ID.Size = new System.Drawing.Size(100, 23);
			TextBox_ID.TabIndex = 2;
			// 
			// TextBox_PW
			// 
			TextBox_PW.Location = new System.Drawing.Point(79, 106);
			TextBox_PW.Name = "TextBox_PW";
			TextBox_PW.Size = new System.Drawing.Size(100, 23);
			TextBox_PW.TabIndex = 4;
			TextBox_PW.UseSystemPasswordChar = true;
			// 
			// label3
			// 
			label3.AutoSize = true;
			label3.Location = new System.Drawing.Point(12, 109);
			label3.Name = "label3";
			label3.Size = new System.Drawing.Size(25, 15);
			label3.TabIndex = 3;
			label3.Text = "PW";
			// 
			// TextBox_Nickname
			// 
			TextBox_Nickname.Location = new System.Drawing.Point(79, 135);
			TextBox_Nickname.Name = "TextBox_Nickname";
			TextBox_Nickname.Size = new System.Drawing.Size(100, 23);
			TextBox_Nickname.TabIndex = 6;
			// 
			// label4
			// 
			label4.AutoSize = true;
			label4.Location = new System.Drawing.Point(12, 138);
			label4.Name = "label4";
			label4.Size = new System.Drawing.Size(61, 15);
			label4.TabIndex = 5;
			label4.Text = "Nickname";
			// 
			// Button_Register
			// 
			Button_Register.Location = new System.Drawing.Point(12, 164);
			Button_Register.Name = "Button_Register";
			Button_Register.Size = new System.Drawing.Size(167, 47);
			Button_Register.TabIndex = 7;
			Button_Register.Text = "회원가입";
			Button_Register.UseVisualStyleBackColor = true;
			// 
			// RegisterPage
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			ClientSize = new System.Drawing.Size(192, 223);
			Controls.Add(Button_Register);
			Controls.Add(TextBox_Nickname);
			Controls.Add(label4);
			Controls.Add(TextBox_PW);
			Controls.Add(label3);
			Controls.Add(TextBox_ID);
			Controls.Add(label2);
			Controls.Add(label1);
			Name = "RegisterPage";
			Text = "RegisterPage";
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox TextBox_ID;
		private System.Windows.Forms.TextBox TextBox_PW;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox TextBox_Nickname;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button Button_Register;
	}
}