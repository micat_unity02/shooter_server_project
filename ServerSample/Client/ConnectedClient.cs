﻿using Core;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerSample.Client;

public class ConnectedClient
{
    /// <summary>
    /// 연결된 클라이언트 소켓 객체를 나타냅니다.
    /// </summary>
    public Socket clientSocket { get; private set; }

    /// <summary>
	/// 이 클라이언트의 연결이 종료되었을 경우 발생하는 이벤트
	/// </summary>
    public event Action<ConnectedClient> onDisconnected;

	/// <summary>
	/// 이 객체에서 서버쪽으로 쿼리문 요청시에 사용될 함수
	/// </summary>
	public event Action<string, Func<MySqlDataReader, Task>> requestQueryCmd;

	/// <summary>
	/// 공지된 내용이 존재하는 경우 발생하는 이벤트
	/// </summary>
	public event Func<ConnectedClient/*client*/, PacketType/*packetType*/, object/*context*/, Task/*retVal*/> onAnnounced;

	/// <summary>
	/// ConnectedClient 객체에서 서버로 요청을 보내기 위한 이벤트
	/// </summary>
	public event Action<RequestType, RequestToServerBase> RequestToServer;

	public string id { get; private set; }
	public string nickname { get; private set; }
	public bool isLogin => !string.IsNullOrEmpty(id);

	private Queue<(ResponseType type, object context)> _ResponsedFromServer = new();
	public Queue<(ResponseType type, object context)> responsedFromServer
	{
		get
		{
			lock (_ResponsedFromServer)
			{
				return _ResponsedFromServer;
			}
		}
	}

	// 플레이어의 갱신된 위치를 저장할 Dictionary 입니다.
	private Dictionary<string, UpdatedPlayerPosition> _UpdatedPlayerPositions = new();
	public Dictionary<string, UpdatedPlayerPosition> updatedPlayerPositions
	{
		get
		{
			lock (_UpdatedPlayerPositions)
			{
				return _UpdatedPlayerPositions;
			}
		}
	}





	public ConnectedClient(Socket clientSocket)
    {
        this.clientSocket = clientSocket;

		Console.WriteLine("클라이언트 연결됨!" +
            $"[{clientSocket.RemoteEndPoint}]");

        // 클라이언트 데이터 수신 시작
        new Thread(DoListen).Start();
    }
	

    public async void DoListen()
    {
        try
        {
            while (true)
            {
				while(IsResponseExist())
				{
					// 요청에 대한 응답을 하나씩 얻습니다.
					(ResponseType type, object context) response = GetResponse();

					if (response.type == ResponseType.Response_PlayerList)
					{
						// 서버의 응답 결과를 얻습니다.
						PlayerListResponsePacket packet = (PlayerListResponsePacket)response.context;

						// 응답 결과를 클라이언트로 보냅니다.
						await Packet.SendAsync(clientSocket, PacketType.PlayerListResponse, packet);
					}

					// 새로운 유저가 연결된 경우
					else if (response.type == ResponseType.Notify_NewPlayerConnection)
					{
						NewPlayerConnectionPacket packet = (NewPlayerConnectionPacket)response.context;

						// 클라이언트에 보냅니다.
						await Packet.SendAsync(clientSocket, PacketType.NewPlayerConnected, packet);

					}

					// 유저가 접속을 종료한 경우
					else if (response.type == ResponseType.Notify_PlayerDisconnected)
					{
						// 서버의 알림 내용을 얻습니다.
						PlayerDisconnectedPacket packet = (PlayerDisconnectedPacket)response.context;

						// 연결된 클라이언트로 보냅니다.
						await Packet.SendAsync(clientSocket, PacketType.PlayerDisconnected, packet);
					}

					// 플레이어 위치 갱신 알림을 받은 경우
					else if (response.type == ResponseType.Notify_PlayerPositionUpdated)
					{
						UpdatePlayerPositionRequestPacket packet = 
							(UpdatePlayerPositionRequestPacket)response.context;

						// 갱신된 위치 기록
						updatedPlayerPositions[packet.nickname] = new UpdatedPlayerPosition(
							packet.nickname,
							packet.position,
							packet.forward);
					}
				}


				// 패킷을 받습니다.
				PacketType packetType = await Packet.ReceiveAsyncPacketType(clientSocket);

				// 연결 끊김
				if (packetType == PacketType.IsDisconnected)
				{
					RequestToServer(RequestType.Request_PlayerDisconnect, new RequestSimple(this));
					onDisconnected?.Invoke(this);
					break;
				}

				// 계정 생성 요청
				else if (packetType == PacketType.CreateAccountRequest)
				{
					CreateAccountRequestPacket packet = 
						await Packet.ReceiveAsyncHeaderAndData<CreateAccountRequestPacket>(clientSocket);

					Console.WriteLine($"계정 생성 요청됨! id = {packet.id} / nickname = {packet.nickname}");

					// 중복된 id / nickname 이 존재하는지 확인합니다.
					requestQueryCmd(
						$"SELECT * FROM {Constants.DB_SCHEMANAME}.{Constants.TABLE_ACCOUNTS} " +
						$"WHERE id = '{packet.id}' || nickname = '{packet.nickname}';",
						async (table) =>
						{
							// 중복 여부를 나타냅니다.
							bool isDuplicated = false;
							string context = null;

							while(table.Read())
							{
								if (isDuplicated) break;

								isDuplicated = true;

								string readID = table["id"].ToString();
								string readNickname = table["nickname"].ToString();

								if (packet.id == readID)
									context = $"{readID}는 사용할 수 없는 ID입니다.";
								else if (packet.nickname == readNickname)
									context = $"{readNickname}은 사용할 수 없는 닉네임입니다.";
							}

							if (!isDuplicated)
							{
								// 요청된 내용을 Table 에 추가합니다.
								requestQueryCmd(
									$"INSERT INTO {Constants.DB_SCHEMANAME}.{Constants.TABLE_ACCOUNTS} (id, pw, nickname) VALUES " +
									$"('{packet.id}', '{packet.pw}', '{packet.nickname}');", null);
								Console.WriteLine($"{packet.id} 계정 생성 성공.");
							}
							else Console.WriteLine($"{packet.id} 계정 생성 실패.");

							// 응답 결과를 생성합니다.
							SimpleResponsePacket responsePacket = new SimpleResponsePacket(
								!isDuplicated, context);

							// 응답 결과를 보냅니다.
							await Packet.SendAsync(clientSocket, 
								PacketType.CreateAccountResponse, responsePacket);
						});

				}

				// 로그인 요청
				else if (packetType == PacketType.LoginRequest)
				{
					LoginRequestPacket packet = 
						await Packet.ReceiveAsyncHeaderAndData<LoginRequestPacket>(clientSocket);

					Console.WriteLine($"로그인 요청됨 {packet.id}");

					// 요청된 id/pw 와 일치하는 데이터가 있는지 조회
					bool finded = false;
					requestQueryCmd(
						$"SELECT * FROM {Constants.DB_SCHEMANAME}.{Constants.TABLE_ACCOUNTS} " +
						$"WHERE id = '{packet.id}' && pw = '{packet.pw}';",
						async(table) =>
						{
							string context = null;
							while (table.Read())
							{
								finded = true;
								this.id = packet.id;
								this.nickname = table["nickname"].ToString();
							}
							if (!finded) context = $"ID와 PW를 확인하세요.";


							// 응답 결과를 생성합니다.
							LoginResponsePacket responsePacket = new(finded, packet.id, nickname, context);

							// 응답 결과를 보냅니다.
							await Packet.SendAsync(clientSocket,
								PacketType.LoginResponse, responsePacket);
						});
				}

				// 이 클라이언트가 로그인 이후, 월드에 연결됨
				else if (packetType == PacketType.ConnectToWorldRequest)
				{
					SimpleRequestPacket packet =
						await Packet.ReceiveAsyncHeaderAndData<SimpleRequestPacket>(clientSocket);

					// 응답을 보냅니다.
					await Packet.SendAsync(clientSocket, PacketType.ConnectToWorldResponse, 
						new SimpleResponsePacket());

					// 서버에 알립니다.
					RequestToServer(RequestType.Request_NewPlayerConnection, new RequestSimple(this));
				}

				// 접속된 플레이어 리스트 요청
				else if (packetType == PacketType.PlayerListRequest)
				{
					// 요청을 받습니다.
					PlayerListRequestPacket requestPacket =
						await Packet.ReceiveAsyncHeaderAndData<PlayerListRequestPacket>(clientSocket);
					Console.WriteLine($"{requestPacket.nickname}에게 플레이어 리스트를 요청받았음.");

					// 플레이어 리스트를 서버에게 요청합니다.
					RequestToServer(RequestType.Request_PlayerList, new RequestSimple(this));

					// 서버의 응답을 대기합니다.
					while (!IsResponseExist()) ;
				}

				// 플레이어 위치 갱신 요청을 받은 경우
				else if (packetType == PacketType.UpdatePlayerPositionRequest)
				{
					UpdatePlayerPositionRequestPacket packet =
						await Packet.ReceiveAsyncHeaderAndData<UpdatePlayerPositionRequestPacket>(clientSocket);

					// 서버로 위치 갱신 요청을 보냅니다.
					RequestToServer(RequestType.Request_UpdatePlayerPosition,
						new RequestUpdatePlayerPosition(this, packet.position, packet.forward));
				}



				// 온라인 상태 갱신 요청
				else if (packetType == PacketType.UpdateOnlineStateRequest)
				{
					SimpleRequestPacket requestPacket = 
						await Packet.ReceiveAsyncHeaderAndData<SimpleRequestPacket>(clientSocket);

					// 갱신시켜야 하는 플레이어 위치가 존재하는 경우
					if (updatedPlayerPositions.Count != 0)
					{
						// 보낼 패킷
						UpdatedPlayerPositionsPacket packet = new(
							new List<UpdatedPlayerPosition>(updatedPlayerPositions.Values));
						updatedPlayerPositions.Clear();

						await Packet.SendAsync(clientSocket, 
							PacketType.UpdatedPlayerPositionResponse, packet);

					}
				}
            }
        }
        catch (ObjectDisposedException)
        {
			Console.WriteLine("ObjectDisposedException = ");
            // 클라이언트 연결 해제
            onDisconnected?.Invoke(this);
        }
        //catch (Exception e)
		//{
		//	Console.WriteLine($"Exception = {e.GetType().Name}");
		//	// 클라이언트 연결 해제
		//	onDisconnected?.Invoke(this);
        //}
    }

	public bool IsResponseExist()
	{
		return responsedFromServer.Count != 0;
	}

	public (ResponseType type, object context) GetResponse()
	{
		return responsedFromServer.Dequeue();
	}

	public void OnResponsedFromServer(ResponseType responseType, object data)
	{
		responsedFromServer.Enqueue(new(responseType, data));
	}
}
