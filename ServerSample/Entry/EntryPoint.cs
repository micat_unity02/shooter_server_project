﻿using Core;
using MySql.Data.MySqlClient;
using ServerSample.Client;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace ServerSample;

// 네트워크
// 서로 다른 위치에 있는 기기를 여러가지 통신 매체를 이용하여 연결시켜 놓은 것

// Socket
// 물리적으로 잘 연결되어있는 네트워크상에서 데이터 송수신에 사용할 수 있는 소프트웨어적 장치

// TODO
// Packet.cs:line 137


public class EntryPoint
{

	// 소켓 객체
	private Socket _ServerSocket;

	// 연결된 클라이언트 객체들을 나타냅니다.
	private List<ConnectedClient> _Clients = new();
	public List<ConnectedClient> clients 
	{
		get 
		{ 
			lock(_Clients)
			{
				return _Clients;
			}
		}
	}

	/// <summary>
	/// MySql 연결 객체
	/// </summary>
	private MySqlConnection _MySqlConnection;

	/// <summary>
	/// 요청된 쿼리문
	/// </summary>
	private Queue<RequestQueryCommand> _RequestQueryCommands = new();


	private Queue<ConnectedClient> _ConnectRequests = new();
	private bool _ConnectRequestInProgress = false;

	/// <summary>
	/// ConnectedClient 를 통해 받은 요청들을 담아둘 큐
	/// </summary>
	private Queue<(RequestType type, RequestToServerBase context)> _RequestsFromClient = new();

	public Queue<(RequestType type, RequestToServerBase context)> requestsFromClient
	{
		get
		{
			lock(_RequestsFromClient)
			{
				return _RequestsFromClient;
			}
		}
	}

	/// <summary>
	/// ConnectedClient 에서 보낸 요청을 처리하기 위한 태스크
	/// </summary>
	private Task _ResponseProcedureTask;




	private static async Task Main() 
		=> await new EntryPoint().WaitNewConnection();

	public EntryPoint()
	{
		IPEndPoint endPoint = new(IPAddress.Parse(
			Core.Constants.ADDRESS), Core.Constants.SERVERPORT);

		/*
		 TCP : (연결지향형)
		 - 데이터의 순서와 도착을 보장합니다. (신뢰성 보장)
		 - 1 : 1 연결
		 - 게임 / 채팅 프로그램에서 주로 사용됩니다.

		 UDP : (비 연결지향형)
		 - 데이터의 순서, 도착을 보장하지 않습니다. (신뢰성을 보장받지 못함)
		 - TCP 보다 빠릅니다.
		 - 1 : N 연결
		 - 보통 라이브 스트리밍에서 사용됩니다.
		*/
		_ServerSocket = new(
			AddressFamily.InterNetwork, 
			SocketType.Stream, 
			ProtocolType.Tcp);

		_ServerSocket.Bind(endPoint);
		_ServerSocket.Listen(15);

		_ResponseProcedureTask = Task.Factory.StartNew(ResponseProcedure);
	}

	/// <summary>
	/// 새로운 연결을 대기합니다.
	/// </summary>
	/// <returns></returns>
	private async Task WaitNewConnection()
	{
		//using (_MySqlConnection = new(
		//	$"Server=localhost;" +
		//	$"Database={Constants.DB_SCHEMANAME};" +
		//	$"Uid={Constants.DB_ID};" +
		//	$"Pwd={Constants.DB_PW}"))

		using (_MySqlConnection = new(
			$"Server=121.141.17.150;" +
			$"Database={Constants.DB_SCHEMANAME};" +
			$"Uid=BCLASS;" +
			$"Pwd={Constants.DB_PW}"))
		{
			_MySqlConnection.Open();

			// 쿼리 명령어 프로시저 실행
			Thread queryCmdProc = new(QueryCommandProcedure);
			queryCmdProc.Start();

			// 새로운 접속 요청을 받습니다.
			while (true)
			{
				// 연결 요청된 새로운 클라이언트의 연결 요청을 수락합니다.
				Socket connectedClientSocket = await _ServerSocket.AcceptAsync();

				// 클라이언트 연결 요청을 수락하기 위하여 쓰레드 풀에 등록합니다.
				ThreadPool.QueueUserWorkItem(OnClientConnected, connectedClientSocket);
			}
		}
	}

	/// <summary>
	/// 클라이언트 데이터를 수신합니다.
	/// </summary>
	/// <param name="sender"></param>
	private void OnClientConnected(object sender)
	{
		// 연결된 클라이언트 소켓 객체를 얻습니다.
		Socket clientSocket = sender as Socket;

		// 연결된 클라이언트 객체
		ConnectedClient newConnection = new ConnectedClient(clientSocket);

		// 연결 해제 이벤트 등록
		newConnection.onDisconnected += OnClientDisconnected;

		// 쿼리문 요청을 위한 함수 등록
		newConnection.requestQueryCmd += RequestQueryCmd;

		// 공지 함수 등록
		//newConnection.onAnnounced += OnAnnounced;

		// 서버에 요청하는 함수 등록
		newConnection.RequestToServer += OnRequestedFromClient;

		// 연결된 클라이언트 객체를 리스트에 보관합니다.
		clients.Add(newConnection);
	}

	/// <summary>
	/// 하나의 클라이언트가 연결 해제될 경우 호출되는 콜백
	/// </summary>
	/// <param name="disconnectedClient"></param>
	private async void OnClientDisconnected(ConnectedClient disconnectedClient)
	{
		Console.WriteLine("연결이 종료되었습니다." +
			$"[{disconnectedClient.clientSocket.RemoteEndPoint}]");

		// 연결 해제
		disconnectedClient.clientSocket.Shutdown(SocketShutdown.Send);

		// 연결된 클라이언트 목록에서 제거합니다.
		clients.Remove(disconnectedClient);
	}

	private async void QueryCommandProcedure()
	{
		while(true)
		{
			// 처리할 요청이 존재하는지 확인합니다.
			// 처리할 요청이 없다면, 대기합니다.
			if (_RequestQueryCommands.Count == 0) continue;

			// 요청을 얻습니다.
			RequestQueryCommand requestQuery = _RequestQueryCommands.Dequeue();

			// 요청된 명령을 생성합니다.
			MySqlCommand cmd = new(requestQuery.queryCommand, _MySqlConnection);

			if (requestQuery.isNonQuery)
			{
				cmd.ExecuteNonQuery();
			}
			else
			{
				MySqlDataReader table = cmd.ExecuteReader();
				await requestQuery.onExecuteReader(table);
				await table.CloseAsync();
			}
		}
	}

	private void RequestQueryCmd(
		string queryCommand,
		System.Func<MySqlDataReader, Task> onExecuteReader = null)
	{
		// 다른 쓰레드에서 _ReqeustedQueryCommands 에 대한 동시 접근을 제한합니다.
		lock (_RequestQueryCommands)
		{
			_RequestQueryCommands.Enqueue(
				new RequestQueryCommand(queryCommand, onExecuteReader));
		}
	}


	/// <summary>
	/// ConnectedClient를 통해 요청한 경우 호출되는 메서드입니다.
	/// </summary>
	/// <param name="requestType"></param>
	/// <param name="context"></param>
	public void OnRequestedFromClient(RequestType requestType, RequestToServerBase context)
	{
		requestsFromClient.Enqueue(new(requestType, context));
	}

	private void ResponseProcedure()
	{
		while (true)
		{
			if (requestsFromClient.Count == 0) continue;

			// 등록된 요청을 얻습니다.
			(RequestType type, RequestToServerBase context) request = requestsFromClient.Dequeue();
			ConnectedClient client = request.context.orderedBy;

			// 플레이어 리스트를 요청한 경우
            if (request.type == RequestType.Request_PlayerList)
            {
				string debug = $"{client.nickname} 에게 보낼 접속된 유저 목록 : ";

				List<string> playerList = new();
				foreach(ConnectedClient connectedClient in clients)
				{
					if (connectedClient == client) continue;
					else if (!connectedClient.isLogin) continue;

					debug += $"/ {connectedClient.nickname} ";
					playerList.Add(connectedClient.nickname);
				}
				Console.WriteLine(debug);
				
				PlayerListResponsePacket reponsePacket = new PlayerListResponsePacket(
					playerList);

				client.OnResponsedFromServer(
					ResponseType.Response_PlayerList,
					reponsePacket);
			}

			// 월드에 새로운 유저가 접속함을 다른 유저에게 알리도록 요청한 경우

			// 월드에 새로운 유저가 접속함을 다른 유저에게 알리는 경우
			else if (request.type == RequestType.Request_NewPlayerConnection)
			{
				NewPlayerConnectionPacket notifyPacket = new NewPlayerConnectionPacket(
					client.nickname);

				foreach(ConnectedClient connectedClient in clients)
				{
					if (connectedClient == client) continue;
					else if (!connectedClient.isLogin) continue;


					connectedClient.OnResponsedFromServer(
						ResponseType.Notify_NewPlayerConnection,
						notifyPacket);
				}
			}

			// 유저가 접속을 종료한 경우
			else if (request.type == RequestType.Request_PlayerDisconnect)
			{
				PlayerDisconnectedPacket notifyPacket = new PlayerDisconnectedPacket(
					client.nickname);

				foreach(ConnectedClient connectedClient in clients)
				{
					if (connectedClient == client) continue;
					else if (!connectedClient.isLogin) continue;

					connectedClient.OnResponsedFromServer(
						ResponseType.Notify_PlayerDisconnected,
						notifyPacket);
				}
			}

			// 위치 갱신 요청인 온 경우
			else if (request.type == RequestType.Request_UpdatePlayerPosition)
			{
				foreach(ConnectedClient connectedClient in clients)
				{
					if (connectedClient == client) continue;
					else if (!connectedClient.isLogin) continue;

					RequestUpdatePlayerPosition requestUpdatePlayerPosition =
						request.context as RequestUpdatePlayerPosition;

					UpdatePlayerPositionRequestPacket notifyPacket = new UpdatePlayerPositionRequestPacket(
						client.nickname,
						requestUpdatePlayerPosition.position, requestUpdatePlayerPosition.forward);

					// 연결된 클라이언트로 위치가 갱신된 플레이어 닉네임과 위치, 방향 정보를 보냅니다.
					connectedClient.OnResponsedFromServer(
						ResponseType.Notify_PlayerPositionUpdated,
						notifyPacket);
				}
			}
        }
	}

}
