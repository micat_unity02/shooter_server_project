﻿using ServerSample.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSample
{
	public enum RequestType : short
	{
		Request_PlayerList,				// 플레이어 리스트 요청
		Request_NewPlayerConnection,	// 새로운 플레이어 연결 요청
		Request_PlayerDisconnect,		// 유저 접속 종료
		Request_UpdatePlayerPosition,	// 위치 갱신 요청
	}

	public enum ResponseType : short
	{
		Response_PlayerList,			// 플레이어 리스트 응답

		// 공지 성격이 강한 응답
		Notify_NewPlayerConnection,		// 새로운 플레이어 연결됨 알림
		Notify_PlayerDisconnected,      // 유저 접속 종료 알림
		Notify_PlayerPositionUpdated,	// 플레이어 위치 갱신 알림
	}


	public class RequestToServerBase
	{
		public ConnectedClient orderedBy { get; private set; }

		public RequestToServerBase(ConnectedClient orderedBy)
		{
			this.orderedBy = orderedBy;
		}
	}
}
