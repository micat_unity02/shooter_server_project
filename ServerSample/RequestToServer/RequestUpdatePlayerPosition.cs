﻿using Core;
using ServerSample.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSample
{
	public class RequestUpdatePlayerPosition : RequestToServerBase
	{
		public Vector3D position { get; set; }
		public Vector3D forward { get; set; }

		public RequestUpdatePlayerPosition(
			ConnectedClient orderedBy, Vector3D position, Vector3D forward) : base(orderedBy)
		{
			this.position = position;
			this.forward = forward;
		}
	}
}
