﻿using Org.BouncyCastle.Asn1.Ocsp;
using ServerSample.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerSample
{
	public class RequestSimple : RequestToServerBase
	{
		public RequestSimple(ConnectedClient orderedBy) : base(orderedBy) { }
	}
}
