﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct PlayerListRequestPacket
	{
		public string nickname;

		public PlayerListRequestPacket(string nickname)
		{
			this.nickname = nickname;

		}

	}
}
