﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct UpdatedPlayerPosition
	{
		public string nickname;
		public Vector3D position;
		public Vector3D forward;

		public UpdatedPlayerPosition(string nickname, Vector3D position, Vector3D forward)
		{
			this.nickname = nickname;
			this.position = position;
			this.forward = forward;
		}
	}


	public struct UpdatedPlayerPositionsPacket
	{
		public List<UpdatedPlayerPosition> playerPositions;

		public UpdatedPlayerPositionsPacket(
			List<UpdatedPlayerPosition> playerPositions)
		{
			this.playerPositions = playerPositions;
		}
	}
}
