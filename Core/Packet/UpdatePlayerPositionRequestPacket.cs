﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct UpdatePlayerPositionRequestPacket
	{
		public string nickname;
		public Vector3D position;
		public Vector3D forward;

		public UpdatePlayerPositionRequestPacket(string nickname, Vector3D position, Vector3D forward)
		{
			this.nickname = nickname;
			this.position = position;
			this.forward = forward;
		}
	}
}
