﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct LoginResponsePacket
	{
		public bool result;
		public string id;
		public string nickname;
		public string context;

		public LoginResponsePacket(bool result, string id, string nickname, string context)
		{
			this.result = result;
			this.id = id;
			this.nickname = nickname;
			this.context = context;
		}
	}
}
