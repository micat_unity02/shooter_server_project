﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct PlayerDisconnectedPacket
	{
		public string nickname;

		public PlayerDisconnectedPacket(string nickname)
		{ 
			this.nickname = nickname;
		}
	}
}
