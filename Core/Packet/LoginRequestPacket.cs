﻿using System;

namespace Core
{
	public struct LoginRequestPacket
	{
		public string id;
		public string pw;

		public LoginRequestPacket(string id, string pw)
		{
			this.id = id;
			this.pw = pw;
		}

		public bool IsLoginable(out string context)
		{
			bool isIdEmpty = string.IsNullOrEmpty(id);
			bool isPwEmpty = string.IsNullOrEmpty(pw);

			// Check Text Empty
			if (isIdEmpty)
			{
				context = "ID는 비울 수 없습니다.";
				return false;
			}
			else if (isPwEmpty)
			{
				context = "PW는 비울 수 없습니다.";
				return false;
			}
			else if (id.Length < Constants.MIN_IDLENGTH)
			{
				context = $"{Constants.MIN_IDLENGTH}글자 미만의 ID는 사용되지 않습니다.";
				return false;
			}
			else if (pw.Length < Constants.MIN_PWLENGTH)
			{
				context = $"{Constants.MIN_PWLENGTH}글자 미만의 PW는 사용되지 않습니다.";
				return false;
			}

			context = null;
			return true;
		}

	}
}
