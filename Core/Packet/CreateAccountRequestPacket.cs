﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	/// <summary>
	/// 회원가입 요청을 위한 패킷 데이터 형태입니다.
	/// </summary>
	public struct CreateAccountRequestPacket
	{
		public string id;
		public string pw;
		public string nickname;

		public CreateAccountRequestPacket(string id, string pw, string nickname)
		{
			this.id = id;
			this.pw = pw;
			this.nickname = nickname;
		}

		/// <summary>
		/// 가입 가능한 정보로 작성되어있는지 확인합니다.
		/// </summary>
		/// <param name="context">가입 불가능인 경우, 이유가 전달됩니다.</param>
		/// <returns>가입 가능 여부를 반환합니다.</returns>
		public bool IsRegisterable(out string context)
		{
			bool isIdEmpty = string.IsNullOrEmpty(id);
			bool isPwEmpty = string.IsNullOrEmpty(pw);
			bool isNicknameEmpty = string.IsNullOrEmpty(nickname);

			// Check Text Empty
			if (isIdEmpty)
			{
				context = "ID는 비울 수 없습니다.";
				return false;
			}
			else if (isPwEmpty)
			{
				context = "PW는 비울 수 없습니다.";
				return false;
			}
			else if (isNicknameEmpty)
			{
				context = "닉네임은 비울 수 없습니다.";
				return false;
			}

			// Check Length
			else if (id.Length < Constants.MIN_IDLENGTH)
			{
				context = $"{Constants.MIN_IDLENGTH}글자 미만의 ID 를 사용할 수 없습니다.";
				return false;
			}
			else if (id.Length > Constants.MAX_IDLENGTH)
			{
				context = $"{Constants.MAX_IDLENGTH}글자를 초과하는 ID 를 사용할 수 없습니다.";
				return false;
			}
			else if (pw.Length < Constants.MIN_PWLENGTH)
			{
				context = $"{Constants.MIN_IDLENGTH}글자 미만의 PW 를 사용할 수 없습니다.";
				return false;
			}
			else if (pw.Length > Constants.MAX_PWLENGTH)
			{
				context = $"{Constants.MAX_PWLENGTH}글자를 초과하는 PW 를 사용할 수 없습니다.";
				return false;
			}
			else if (nickname.Length < Constants.MIN_NICKNAMELENGTH)
			{
				context = $"{Constants.MIN_NICKNAMELENGTH}글자 미만의 닉네임을 사용할 수 없습니다.";
				return false;
			}
			else if (pw.Length > Constants.MAX_NICKNAMELENGTH)
			{
				context = $"{Constants.MAX_NICKNAMELENGTH}글자를 초과하는 닉네임을 사용할 수 없습니다.";
				return false;
			}

			context = null;
			return true;
		}
	}

}
