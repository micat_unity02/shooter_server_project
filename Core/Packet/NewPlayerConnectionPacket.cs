﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct NewPlayerConnectionPacket
	{
		public string nickname;

		public NewPlayerConnectionPacket(string nickname)
		{
			this.nickname = nickname;
		}
	}
}
