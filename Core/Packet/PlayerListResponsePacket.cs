﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct PlayerListResponsePacket
	{
		public List<string> playerList;

		public PlayerListResponsePacket(List<string> playerList)
		{
			this.playerList = playerList;
		}
	}
}
