﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{ 

	public class Constants
	{

		// 아이피
		public const string ADDRESS = "192.168.3.90";

		// 포트
		public const int SERVERPORT = 20000;

		// 데이터베이스 이름
		public const string DB_SCHEMANAME = "shooter_db";

		// 계정 관리용 테이블명
		public const string TABLE_ACCOUNTS = "accounts";

		// 데이터베이스 ID
		public const string DB_ID = "root";

		// 데이터베이스 PW
		public const string DB_PW = "root";

		// 아이디 길이
		public const int MIN_IDLENGTH = 4;
		public const int MAX_IDLENGTH = 20;

		// 비밀번호 길이
		public const int MIN_PWLENGTH = 4;
		public const int MAX_PWLENGTH = 30;

		// 닉네임 길이
		public const int MIN_NICKNAMELENGTH = 2;
		public const int MAX_NICKNAMELENGTH = 12;
	}
}