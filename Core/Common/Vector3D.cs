﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	public struct Vector3D
	{
		public float x;
		public float y;
		public float z;

		public Vector3D(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override string ToString()
		{
			return $"({x}, {y}, {z})";
		}
	}
}
