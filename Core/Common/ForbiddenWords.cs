﻿using System;
using System.Collections.Generic;

namespace Core
{
	/// <summary>
	/// 금칙어 관련 기능을 제공하는 객체입니다.
	/// </summary>
	public static class ForbiddenWords
	{
		// 금칙어 명단
		private static Dictionary<string /*forbidden word*/, string /*filtered word*/> _ForbiddenWords;

		static ForbiddenWords()
		{
			_ForbiddenWords = new Dictionary<string, string>();

			#region 금칙어와 관련된 내용을 초기화합니다.

			_ForbiddenWords.Add("똥", "뿅");
			_ForbiddenWords.Add("병신", "부족한");
			_ForbiddenWords.Add("개새끼", "아기 강아지");
			_ForbiddenWords.Add("시발", "아잉");

			#endregion
		}

		/// <summary>
		/// 금칙어 포함 여부를 확인합니다.
		/// </summary>
		/// <param name="checkString">검사할 문자열을 전달합니다.</param>
		/// <param name="forbiddenWords">확인된 금칙어들을 저장할 리스트를 전달합니다.</param>
		/// <returns></returns>
		public static bool Exist(string checkString, out List<string> forbiddenWords)
		{
			forbiddenWords = new List<string>();

			// 금칙어가 포함되어있는지 확인합니다.
			foreach(string forbiddenWord in _ForbiddenWords.Keys)
			{
				// 금칙어가 포함되어있다면 리스트에 추가
				if (checkString.Contains(forbiddenWord))
					forbiddenWords.Add(forbiddenWord);
			}

			// 금칙어 포함 여부
			return forbiddenWords.Count != 0;
		}

	}
}
